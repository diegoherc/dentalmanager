@extends('layouts.app')

@section('content')
<h1>Cadastrar Paciente</h1>

<form action="{{ route('patients.store') }}" method="post">
    @csrf
    <div class="form-group">
        <label for="name">Nome</label>
        <input id="name" class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{ old('name') }}">
        @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input id="email" class="form-control @error('email') is-invalid @enderror" type="email" name="email" value="{{ old('email') }}">
        @error('email')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="phone">Telefone</label>
        <input id="phone" class="form-control @error('phone') is-invalid @enderror" type="text" name="phone" value="{{ old('phone') }}">
        @error('phone')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button class="btn btn-primary" type="submit">Cadastrar</button>
    <a class="btn btn-secondary" href="{{ route('patients.index') }}">Voltar</a>
</form>
@endsection
