@extends('layouts.app')

@section('content')
<x-alert />
<form action="" method="GET">
    <div class="form-row align-items-center">
        <div class="col-sm-4 my-1">
            <label class="sr-only" for="inlineFormInputName">Name</label>
            <input type="text" name="search" class="form-control" id="inlineFormInputName" placeholder="João da Silva">
        </div>
        <div class="col-auto my-1">
            <button type="submit" class="btn btn-primary">Buscar</button>
        </div>
    </div>
</form>
<a class="btn btn-primary text-right mb-1" href="{{ route('patients.create') }}">Cadastrar</a>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Telefone</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($patients as $patient)
            <tr>
                <td>{{$patient->id}}</td>
                <td>{{$patient->name}}</td>
                <td>{{$patient->email}}</td>
                <td>{{$patient->phone}}</td>
                <td>
                    <a class="btn btn-primary" href="{{ route('patients.edit', $patient->id) }}">Editar</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{$patients->links()}}
@endsection
