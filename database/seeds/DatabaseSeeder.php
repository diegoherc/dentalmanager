<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(
            [
                'name' => 'John Michael Kennedy',
                'email' => 'test@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('123'),
                'remember_token' => 'haeuheaheauh'
            ]
        );
        $this->call([
            PatientSeeder::class
        ]);
    }
}
