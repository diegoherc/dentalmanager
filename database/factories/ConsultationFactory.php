<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Consultation;
use Faker\Generator as Faker;

$factory->define(Consultation::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'date' => $faker->date,
        'description' => $faker->sentence,
        'value' => $faker->randomFloat(2, 1, 10)
    ];
});
