<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Patient;
use Faker\Generator as Faker;

$factory->define(Patient::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber
    ];
});

$factory->afterCreating(Patient::class, function ($patient) {
    $patient->consultation()->save(factory(App\Consultation::class)->make());
});
