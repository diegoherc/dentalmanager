<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Patient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone',
    ];

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, function ($query, $search) {
            $query->where('name', 'LIKE', "%" . $search . "%");
        });
    }

    public function consultation()
    {
        return $this->hasMany(Consultation::class);
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }
}
