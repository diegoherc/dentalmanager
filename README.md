# DentalManager
This project is developed by Diego Herculano with Laravel framework 7 using the most clean code possible and good practicies.

It is a web application that will manage patients and consultations from dental office.

Features:
- Register Patient and Edit it
- Register Consultation and upload images
- Report consultation by date or patient
- List of patients and consultations with search

That's it for now

_Under construction._
